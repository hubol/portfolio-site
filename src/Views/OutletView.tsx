import {Outlet} from "../types";
import React from "react";

interface Props
{
    outlet: Outlet;
}

export const OutletView: React.FC<Props> = props =>
{
    let { outlet } = props;
    
    return <a href={ outlet.url }>
        <img src={ outlet.icon.url } width={"120px"} />
        { outlet.description }
    </a>;
};