import {Project} from "../types";
import React from "react";

interface Props
{
    project: Project;
    onClick: () => void;
}

export const IconView: React.FC<Props> = props =>
{
    let { project, onClick } = props;
    let { icon, title } = project;
    
    return <div onClick={_ => onClick()}>
        <img src={icon.url} width={"80px"} />
        {title}
    </div>;    
};