import {ContentWarning} from "../types";
import React from "react";
import contentWarningIcon from './Icons/content-warning.svg';

interface Props
{
    contentWarning: ContentWarning;
}

export const ContentWarningView: React.FC<Props> = props =>
{
    let { contentWarning } = props;

    if (contentWarning === 'safe')
        return <></>;

    return <>
        <img src={contentWarningIcon} width={"80px"} />
        { contentWarning.reason }
    </>;
};