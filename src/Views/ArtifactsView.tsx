import React from "react";
import {
    Artifact,
    BandcampAlbum,
    CrossPlatformBinaries,
    Html5Page,
    ItchPage,
    Kind,
    MacOsApplication,
    SelfHostedArtifact,
    WindowsExecutable,
    YouTubeVideo
} from "../types";

import windowsExecutableIconPath from "./Icons/windows-executable.svg";
import macOsApplicationIconPath from "./Icons/macos-application.svg";
import crossPlatformBinariesIconPath from "./Icons/cross-platform-binaries.svg";
import html5PageIconPath from "./Icons/html5-page.svg";

interface Props
{
    kind: Kind,
    artifacts: Array<Artifact>;
}

export const ArtifactsView: React.FC<Props> = props =>
{
    let { artifacts, kind } = props;

    if (artifacts.length === 0)
        return <></>;
    
    let windowsExecutables: Array<SelfHostedArtifact> = artifacts
        .filter((artifact): artifact is WindowsExecutable => artifact.type === "WindowsExecutable");
    
    let macOsApplications: Array<SelfHostedArtifact> = artifacts
        .filter((artifact): artifact is MacOsApplication => artifact.type === "MacOsApplication");
    
    let crossPlatformBinaries: Array<SelfHostedArtifact> = artifacts
        .filter((artifact): artifact is CrossPlatformBinaries => artifact.type === "CrossPlatformBinaries");

    let html5Pages: Array<SelfHostedArtifact> = artifacts
        .filter((artifact): artifact is Html5Page => artifact.type === "Html5Page");

    let selfHostedArtifacts: Array<SelfHostedArtifact> = windowsExecutables
        .concat(macOsApplications)
        .concat(crossPlatformBinaries)
        .concat(html5Pages);

    let bandcampAlbums: Array<BandcampAlbum> = artifacts
        .filter((artifact): artifact is BandcampAlbum => artifact.type === "BandcampAlbum");

    let itchPages: Array<ItchPage> = artifacts
        .filter((artifact): artifact is ItchPage => artifact.type === "ItchPage");

    let youTubeVideos: Array<YouTubeVideo> = artifacts
        .filter((artifact): artifact is YouTubeVideo => artifact.type === "YouTubeVideo");
    
    switch (kind.type)
    {
        case "Album":
            return <>
            { renderBandcampAlbums(bandcampAlbums) }
            { renderYouTubeVideos(youTubeVideos) }
            { renderItchPages(itchPages) }
            { renderSelfHostedArtifacts(selfHostedArtifacts) }
        </>;
        case "Game":
            return <>
            { renderItchPages(itchPages) }
            { renderSelfHostedArtifacts(selfHostedArtifacts) }
            { renderBandcampAlbums(bandcampAlbums) }
            { renderYouTubeVideos(youTubeVideos) }
        </>;
        case "Video":
            return <>
            { renderYouTubeVideos(youTubeVideos) }
            { renderBandcampAlbums(bandcampAlbums) }
            { renderItchPages(itchPages) }
            { renderSelfHostedArtifacts(selfHostedArtifacts) }
        </>;
        case "Performance":
            return <>
            { renderYouTubeVideos(youTubeVideos) }
            { renderBandcampAlbums(bandcampAlbums) }
            { renderItchPages(itchPages) }
            { renderSelfHostedArtifacts(selfHostedArtifacts) }
        </>;
    }
};

function renderYouTubeVideos(youTubeVideos: Array<YouTubeVideo>)
{
    if (youTubeVideos.length === 0)
        return <></>;

    return <ul>
        {
            youTubeVideos.map((selfHostedArtifact, index) =>
                <li key={index}>
                    { renderArtifact(selfHostedArtifact) }
                </li>)
        }
    </ul>;
}

function renderItchPages(itchPages: Array<ItchPage>)
{
    if (itchPages.length === 0)
        return <></>;

    return <ul>
    {
        itchPages.map((selfHostedArtifact, index) =>
            <li key={index}>
                { renderArtifact(selfHostedArtifact) }
            </li>)
    }
    </ul>;
}

function renderBandcampAlbums(bandcampAlbums: Array<BandcampAlbum>)
{
    if (bandcampAlbums.length === 0)
        return <></>;

    return <ul>
    {
        bandcampAlbums.map((selfHostedArtifact, index) =>
            <li key={index}>
                { renderArtifact(selfHostedArtifact) }
            </li>)
    }
    </ul>;
}

function renderSelfHostedArtifacts(selfHostedArtifacts: Array<SelfHostedArtifact>)
{
    if (selfHostedArtifacts.length === 0)
        return <></>;
    
    return <ul> 
    {
        selfHostedArtifacts.map(selfHostedArtifact =>
            <li key={selfHostedArtifact.url}>
                { renderArtifact(selfHostedArtifact) }
            </li>)
    }
    </ul>;
}

function renderArtifact(artifact: Artifact)
{
    switch (artifact.type)
    {
        case "BandcampAlbum":
            return artifact.embeddedPlayer;
        case "WindowsExecutable":
            return renderSelfHostedButton(
                windowsExecutableIconPath,
                "Download a Windows executable.",
                artifact.url);
        case "MacOsApplication":
            return renderSelfHostedButton(
                macOsApplicationIconPath,
                "Download a macOS application. (Limited support)",
                artifact.url);
        case "CrossPlatformBinaries":
            return renderSelfHostedButton(
                crossPlatformBinariesIconPath,
                "Download cross-platform binaries. (Limited support)",
                artifact.url);
        case "Html5Page":
            return renderSelfHostedButton(
                html5PageIconPath,
                "Play in an HTML5-compatible browser.",
                artifact.url);
        case "ItchPage":
            return artifact.embeddedWidget;
        case "YouTubeVideo":
            return artifact.embeddedPlayer;

    }
}

function renderSelfHostedButton(iconPath: string, description: string, url: string)
{
    return <a href={url}>
        <img src={iconPath} width={"128px"}/>
        {description}
    </a>;
}