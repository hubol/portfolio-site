import {Preview} from "../types";
import React from "react";

import ImageZoom from "react-medium-image-zoom";

interface Props
{
    previews: Array<Preview>;
}

export const PreviewsView: React.FC<Props> = props =>
{
    let { previews } = props;
    
    let previewsCopy = [...previews];
    
    if (previewsCopy.length === 0)
        return <></>;
    
    let firstPreview = previewsCopy.shift() as Preview;
    
    return <>
        <div>
            <ImageZoom
                image={
                {
                    src: firstPreview.url,
                    style: { width: "180px" }
                }}
            />
        </div>
        {
            previewsCopy.length > 0 &&
                <div>
                {
                    previewsCopy.map((preview, index) =>
                        <ImageZoom
                            key={`${index}_${preview.url}`}
                            image={
                            {
                                src: preview.url,
                                style: {width: "60px"}
                            }}
                        />)
                }
                </div>
        }
    </>;
};