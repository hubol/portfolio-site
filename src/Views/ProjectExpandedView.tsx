import React from "react";
import {Project} from "../types";
import {ReleaseStatusView} from "./ReleaseStatusView";
import {DescriptionView} from "./DescriptionView";
import {ContentWarningView} from "./ContentWarningView";
import {ContributorsView} from "./ContributorsView";
import {KindView} from "./KindView";
import {ArtifactsView} from "./ArtifactsView";
import {PreviewsView} from "./PreviewsView";

interface Props
{
    project: Project;
}

export const ProjectExpandedView: React.FC<Props> = props =>
{
    let { project } = props;
    
    let {
        kind,
        contributors,
        contentWarning,
        description,
        releaseStatus,
        title,
        artifacts,
        previews
        } = project;
    
    return <div>
        <h1>{title}</h1>
        <ReleaseStatusView releaseStatus={releaseStatus}/>
        <PreviewsView previews={previews}/>
        <KindView kind={kind}/>
        <DescriptionView description={description}/>
        <ContributorsView contributors={contributors}/>
        <ContentWarningView contentWarning={contentWarning}/>
        <ArtifactsView kind={kind} artifacts={artifacts}/>
    </div>;
};