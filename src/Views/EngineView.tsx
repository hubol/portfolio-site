import React from "react";
import {Engine} from "../types";

import gameMakerStudioIcon from "./Icons/game-maker-studio.svg";
import gameMakerSevenIcon from "./Icons/game-maker-7.svg";
import javaIcon from "./Icons/java.svg";
import unityIcon from "./Icons/unity.svg";

interface Props
{
    engine: Engine;
}

export const EngineView: React.FC<Props> = props =>
{
    let { engine } = props;
    
    let {iconUrl, notes} = getEngineProperties(engine);
    
    return <>
        <img src={iconUrl} width={"128px"}/>
        <p><small>{notes}</small></p>
    </>;
};

interface EngineProperties
{
    iconUrl: string;
    notes: string;
}

function getEngineProperties(engine: Engine): EngineProperties
{
    switch (engine)
    {
        case "Asshat V1":
            return { iconUrl: javaIcon, notes: "Made in Java 6. (Supports Windows with limited macOS and Linux support!)"};
        case "Asshat V2":
            return { iconUrl: javaIcon, notes: "Made in Java 8. (Supports Windows with limited macOS and Linux support!)"};
        case "Game Maker 7":
            return { iconUrl: gameMakerSevenIcon, notes: "Made in Game Maker 7. Windows only."};
        case "Game Maker Studio":
            return { iconUrl: gameMakerStudioIcon, notes: "Made in Game Maker Studio."};
        case "Unity":
            return { iconUrl: unityIcon, notes: "Made in Unity--the industry standard for developing game's."};
    }
}