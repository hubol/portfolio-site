import {Project} from "../types";
import React from "react";
import {connect} from "react-redux";
import {PortfolioAction, PortfolioState} from "../Store/store";
import {getFilteredProjects, getProjectLocationDescriptorObject} from "../Store/projects";
import {Dispatch} from "redux";
import {IconView} from "./IconView";
import {push} from "connected-react-router";

interface Props
{
    filteredProjects: Array<Project>;
    filterCollaborationProjects: boolean;
    filterTrashProjects: boolean;
    filterNonGames: boolean;
    
    toggleCollaborationFilter: () => void;
    toggleTrashFilter: () => void;
    toggleNonGamesFilter: () => void;
    expandProject: (project: Project) => void;
}

export const ProjectsListView: React.FC<Props> = (props) =>
{
    let {
        filterTrashProjects,
        filterCollaborationProjects,
        filterNonGames,
        filteredProjects,
        
        toggleCollaborationFilter,
        toggleTrashFilter,
        toggleNonGamesFilter,
        expandProject} = props;
    
    return <>
        <div onClick={ toggleTrashFilter }>{ !filterTrashProjects ? "Include Trash" : "Exclude Trash" }</div>
        <div onClick={ toggleCollaborationFilter }>{ !filterCollaborationProjects ? "Include Collaborations" : "Exclude Collaborations" }</div>
        <div onClick={ toggleNonGamesFilter }>{ !filterNonGames ? "Include Non-Game Projects" : "Only Game Projects" }</div>
        {
            filteredProjects.map( project =>
                <IconView key={project.title} project={project} onClick={() => expandProject(project)}/>)
        }
    </>;
};

const mapPortfolioState = (portfolioState: PortfolioState) => ({
    filteredProjects: getFilteredProjects(portfolioState.project),
    filterCollaborationProjects: portfolioState.project.filterCollaborationProjects,
    filterTrashProjects: portfolioState.project.filterTrashProjects,
    filterNonGames: portfolioState.project.filterNonGameProjects
});

const mapDispatch = (dispatch: Dispatch<PortfolioAction>) => ({
    toggleCollaborationFilter: () => dispatch({type: "ToggleCollaborationFilter"}),
    toggleTrashFilter: () => dispatch({type: "ToggleTrashFilter"}),
    toggleNonGamesFilter: () => dispatch({type: "ToggleNonGamesFilter"}),
    expandProject: (project: Project) => dispatch(push(getProjectLocationDescriptorObject(project))),
});

export const ConnectedProjectsListView = 
  connect(
      mapPortfolioState,
      mapDispatch
  )(ProjectsListView);