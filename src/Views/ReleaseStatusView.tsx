import {ReleaseStatus} from "../types";
import React from "react";
import {getMonth} from "../Utils/DateUtils";

interface Props
{
    releaseStatus: ReleaseStatus;
}

export const ReleaseStatusView: React.FC<Props> = props =>
{
    let { releaseStatus } = props;

    if (releaseStatus === "unreleased")
        return <h2>Unreleased</h2>;
    
    let { date } = releaseStatus;

    let month = getMonth(date);
    return <h2>{`${date.getDate()} ${month} ${date.getFullYear()}`}</h2>
};