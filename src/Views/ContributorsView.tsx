import {ContentWarning, Contributor, Role} from "../types";
import React from "react";
import {distinct, groupBy} from "../Utils/ArrayUtils";

interface Props
{
    contributors: Array<Contributor>;
}

export const ContributorsView: React.FC<Props> = props =>
{
    let { contributors } = props;

    if (contributors.length === 0)
        return <></>;
    
    let normalizedContributors = normalizeContributors(contributors);
    
    return <>
        <h2>Personnel</h2>
        <ul>
        { normalizedContributors.map(renderContributor) }
        </ul>
    </>;
};

function normalizeRoles(roles: Array<Role>): Array<Role>
{
    return distinct(roles, role => role)
        .sort();
}

function normalizeContributors(contributors: Array<Contributor>): Array<Contributor>
{
    let flattenedContributors = groupBy(contributors, contributor => contributor.name)
        .flatMap(group =>
        {
            let roles = group.flatMap(contributor => contributor.roles);
            return {
                name: group[0].name,
                roles
            };
        });

    let hubolContributors = flattenedContributors
        .filter(contributor => contributor.name === "Hubol Persson-Gordon");

    if (hubolContributors.length > 1)
        throw new Error(`Should only have <= 1 Hubol contributors but got ${hubolContributors.length}`);

    let hubolContributor = hubolContributors[0];

    let nonHubolContributors = flattenedContributors
        .filter(contributor => contributor.name !== "Hubol Persson-Gordon");

    nonHubolContributors.push(hubolContributor);

    return nonHubolContributors;
}

function renderContributor(contributor: Contributor)
{
    let normalizedRoles = normalizeRoles(contributor.roles);

    return <li key={contributor.name}>
        {contributor.name}
        {
            normalizedRoles.length > 0 &&
            <ul>
                { normalizedRoles.map( role =>
                    <li key={role}>{role}</li>) }
            </ul>
        }
    </li>;
}