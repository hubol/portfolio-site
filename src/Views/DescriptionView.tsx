import React from "react";
import {Description, Project} from "../types";
import {isString} from "../Utils/TypeGuards";

interface Props
{
    description: Description;
}

export const DescriptionView: React.FC<Props> = props =>
{
    let { description } = props;

    if (isString(description))
        return <p>{description}</p>;
    
    return description;
};