import {Outlet} from "../types";
import * as React from "react";
import {OutletView} from "./OutletView";

interface Props
{
    outlets: Array<Outlet>;
}

export const OutletsListView: React.FC<Props> = props =>
{
    let { outlets } = props;
    
    if (outlets.length === 0)
        return <></>;
    
    return <ul>
        { outlets.map( outlet =>
            <li>
                <OutletView outlet={outlet}/>
            </li> ) }
    </ul>
};