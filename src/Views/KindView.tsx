import React from "react";
import {Engine, Kind} from "../types";

import {EngineView} from "./EngineView";

interface Props
{
    kind: Kind;
}

export const KindView: React.FC<Props> = props =>
{
    let { kind } = props;

    switch (kind.type)
    {
        case "Game":
            return <EngineView engine={kind.engine}/>;
        default:
            return <></>; // TODO handle other cases
    }
};