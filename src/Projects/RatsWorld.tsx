import {Project} from "../types";
import * as React from "react";
import {renderContributor} from "../Utils/ViewUtils";
import {date} from "../Utils/DateUtils";

import ratsWorldScreenshot from "./Previews/ratsworld1.png";
import ratsWorldIcon from "./Icons/rats-world.png";

export const ratsWorld: Project =
{
    title: "Rats World",
    routeName: "ratsworld",
    artifacts:
    [
        { type: "BandcampAlbum", embeddedPlayer:
                <iframe style={{border: "0", width: "100%", height: "120px"}} src="https://bandcamp.com/EmbeddedPlayer/album=4100840206/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="http://hubol.bandcamp.com/album/rats-world-ost">Rats World Ost by Hubol</a></iframe>},
        { type: "Html5Page", url: "http://love-game.net/ratsworld" }
    ],
    contentWarning: "safe",
    contributors:
    [
        { name: "Hubol Persson-Gordon", roles: ["Audio Designer", "Quality Assurance"] },
        { name: "Sylvie", roles: ["Game Designer", "Developer", "Visual Designer"] },
    ],
    description:
        <>
        <blockquote>Rats World. A little world where rats wander and play.... what secrets does it hide?</blockquote>
        <p>An exploration puzzle game by {renderContributor("Sylvie")} featuring Hubolish sound effects and music.</p>
        </>,
    icon: { url: ratsWorldIcon},
    kind: { type: "Game", engine: "Game Maker Studio" },
    previews:
    [
        { url: ratsWorldScreenshot },
        { url: ratsWorldScreenshot },
        { url: ratsWorldScreenshot }
    ],
    releaseStatus: { date: date(5, 1, 2019)  },
    quality: "Not Trash"
};