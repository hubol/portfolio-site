import {ratsWorld} from "./RatsWorld";
import {Project} from "../types";

export const projects: Array<Project> =
[
    ratsWorld,
    { ...ratsWorld, routeName: "whatever1", title: "Whatever1", releaseStatus: {date: new Date(2020, 10 , 1)}},
    { ...ratsWorld, routeName: "whatever2", title: "Whatever2", contributors: [{name: "Hubol Persson-Gordon", roles: ["Developer"]}], releaseStatus: {date: new Date(2021, 10 , 1)}},
    { ...ratsWorld, routeName: "whateve3", title: "Whatever3 NEW", quality: "Trash", releaseStatus: "unreleased"}
];