import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import {createBrowserHistory, History} from "history";
import {CallHistoryMethodAction, connectRouter, routerMiddleware, RouterState} from "connected-react-router";
import {ProjectAction, ProjectState, projectStateReducer} from "./projects";
import {composeWithDevTools} from "redux-devtools-extension";

export const history = createBrowserHistory();

export function initializeStore()
{
    return createStore<PortfolioState, PortfolioAction, any, any>(
        createRootReducer(history),
        composeWithDevTools(
            applyMiddleware(
                routerMiddleware(history)
            )
        )
    );
}

function createRootReducer(history: History)
{
    return combineReducers<PortfolioState>(
        {
            router: connectRouter(history),
            project: projectStateReducer
        }
    );
}

export interface PortfolioState
{
    router: RouterState,
    project: ProjectState
}

export type PortfolioAction =
      ProjectAction
    | CallHistoryMethodAction;

