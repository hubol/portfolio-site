import {Project} from "../types";
import {projects} from "../Projects";
import {RouterState} from "connected-react-router";
import {LocationDescriptorObject} from "history";

export type ProjectAction =
      ToggleCollaborationFilter
    | ToggleTrashFilter
    | ToggleNonGamesFilter;

export interface ToggleCollaborationFilter
{
    type: "ToggleCollaborationFilter";
}

export interface ToggleTrashFilter
{
    type: "ToggleTrashFilter";
}

export interface ToggleNonGamesFilter
{
    type: "ToggleNonGamesFilter";
}

export interface ProjectState
{
    filterCollaborationProjects: boolean;
    filterTrashProjects: boolean;
    filterNonGameProjects: boolean;
}

const initialProjectState: ProjectState =
{
    filterNonGameProjects: false,
    filterCollaborationProjects: true,
    filterTrashProjects: false
};

export const projectStateReducer =
    (state: ProjectState = initialProjectState, action: ProjectAction): ProjectState =>
{
    switch (action.type)
    {
        case "ToggleNonGamesFilter":
            return {
                ...state,
                filterNonGameProjects: !state.filterNonGameProjects
            };
        case "ToggleCollaborationFilter":
            return {
                ...state,
                filterCollaborationProjects: !state.filterCollaborationProjects
            };
        case "ToggleTrashFilter":
            return {
                ...state,
                filterTrashProjects: !state.filterTrashProjects
            };
        default:
            return state;
    }
};

export function getFilteredProjects(projectState: ProjectState): Array<Project>
{
    let { filterCollaborationProjects, filterTrashProjects, filterNonGameProjects } = projectState;

    return projects
        .filter(project => filterCollaborationProjects || isSoloProject(project))
        .filter(project => filterTrashProjects || !isTrashProject(project))
        .filter(project => filterNonGameProjects || isGameProject(project))
        .sort(compareProjectReleaseStatuses);
}

function compareProjectReleaseStatuses(a: Project, b: Project)
{
    if (a.releaseStatus === "unreleased")
        return b.releaseStatus === "unreleased" ? 0 : -1;
    
    if (b.releaseStatus === "unreleased")
        return 1;
    
    return b.releaseStatus.date.getTime() - a.releaseStatus.date.getTime();
}

function isGameProject(project: Project)
{
    return project.kind.type === "Game";
}

function isSoloProject(project: Project)
{
    return project.contributors.length === 1 && project.contributors[0].name === "Hubol Persson-Gordon";
}

function isTrashProject(project: Project)
{
    return project.quality === "Trash";
}

export function getExpandedProject(router: RouterState): Project | null {
    let {pathname, search} = router.location;
    
    let sanitizedSearch = search.replace("?id=", "");

    if (pathname !== "/index.php")
        return null;

    let projectsWithSearch = projects
        .filter(project => project.routeName === sanitizedSearch);

    if (projectsWithSearch.length === 0)
        return null;

    if (projectsWithSearch.length === 1)
        return projectsWithSearch[0];

    throw new Error(`Expected to get exactly 1 Project with routeName=${sanitizedSearch}, but got ${projectsWithSearch.length}`);
}

export const getProjectLocationDescriptorObject = (project: Project): LocationDescriptorObject => ({
    pathname: "/index.php",
    search: `?id=${project.routeName}`
});