import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { history, initializeStore} from "./Store/store";
import { Provider } from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import {ConnectedApp} from "./App";

const store = initializeStore();

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <ConnectedApp />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('root'));

//
// <Route exact path={"/kittyrpg"} render={() => <p>Kitty RPG 2</p>}/>
// <Route exact path={"/index.php"} render={() => {
//     history.push({ pathname: "", search: ""})
//     return wtf(history)}/>
// <Route exact path={"/index.php?id=kittyrpg"} render={() => <p>Kitty RPG 4</p>}/>
//
// function wtf(history: any)
// {
//     debugger;
//     return <p>
//         Kitty RPG 3
//     </p>;
// }

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
