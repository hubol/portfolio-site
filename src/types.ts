import {ReactElement} from "react";

export interface Project
{
    title: string;
    routeName: string;
    kind: Kind;
    icon: Image;
    contentWarning: ContentWarning;
    releaseStatus: ReleaseStatus;
    contributors: Array<Contributor>;
    description: Description;
    previews: Array<Preview>;
    artifacts: Array<Artifact>;
    quality: Quality;
}

export interface Outlet
{
    icon: Image;
    description: string;
    url: string;
}

export type Description = ReactElement | string;

export type ReleaseStatus = Released | Unreleased;

export type Preview = Image;

export type Artifact =
      SelfHostedArtifact
    | BandcampAlbum
    | ItchPage
    | YouTubeVideo;

export type SelfHostedArtifact =
    WindowsExecutable
    | MacOsApplication
    | CrossPlatformBinaries
    | Html5Page;

export type Kind =
      Album
    | Game
    | Video
    | Performance;

export type Role =
      "Developer"
    | "Game Designer"
    | "Audio Designer"
    | "Visual Designer"
    | "Audio Engineer"
    | "Performer"
    | "Quality Assurance";

export type Quality = "Trash" | "Not Trash";

export interface Album
{
    type: "Album";
}

export interface Video
{
    type: "Video";
}

export interface Performance
{
    type: "Performance";
}

export interface Game
{
    type: "Game";
    engine: Engine;
}

export type Engine =
      "Asshat V1"
    | "Asshat V2"
    | "Game Maker 7"
    | "Game Maker Studio"
    | "Unity";

export interface Contributor
{
    name: Name;
    roles: Array<Role>;
}

export interface Released
{
    date: Date;
}

export type Unreleased = "unreleased";

export interface Image
{
    url: string;
}

export type ContentWarning = Safe | Unsafe;

export type Safe = "safe";

export interface Unsafe
{
    reason: string;
}

export type Name =
      "Hubol Persson-Gordon"
    | "Sylvie"
    | "The Oddwarg"
    | "Good Evening Gumm";

export function getWebsiteUrl(name: Name): string
{
    switch (name)
    {
        case "Hubol Persson-Gordon":
            return "www.hubolhubolhubol.com";
        case "Sylvie":
            return "www.love-game.net";
        case "The Oddwarg":
            return "www.oddwarg.com";
        case "Good Evening Gumm":
            return "www.gumm.band";
    }
}

export interface BandcampAlbum
{
    type: "BandcampAlbum";
    embeddedPlayer: ReactElement;
}

export interface WindowsExecutable
{
    type: "WindowsExecutable";
    url: string;
}

export interface MacOsApplication
{
    type: "MacOsApplication";
    url: string;
}

export interface CrossPlatformBinaries
{
    type: "CrossPlatformBinaries";
    url: string;
}

export interface Html5Page
{
    type: "Html5Page";
    url: string;
}

export interface ItchPage
{
    type: "ItchPage";
    embeddedWidget: ReactElement;
}

export interface YouTubeVideo
{
    type: "YouTubeVideo";
    embeddedPlayer: ReactElement;
}