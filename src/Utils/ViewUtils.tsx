import {getWebsiteUrl, Name} from "../types";
import * as React from "react";

export function renderContributor(name: Name)
{
    return <a href={getWebsiteUrl(name)}>{name}</a>;
}