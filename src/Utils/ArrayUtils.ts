import {Dictionary} from "./Dictionary";

/*!
 * Group items from an array together by some criteria or value.
 * (c) 2019 Tom Bremmer (https://tbremer.com/) and Chris Ferdinandi (https://gomakethings.com), MIT License
 */
export function groupBy<T, U extends string>(array: Array<T>, groupKeyMap: (item: T) => U): Array<Array<T>>
{
    let dictionary = array.reduce(function (dict: Dictionary<Array<T>>, item)
    {
        let key = groupKeyMap(item);
        
        if (!dict.hasOwnProperty(key))
            dict[key] = [];
        
        dict[key].push(item);
        
        return dict;

    }, {}) as any;
    
    let resultArray = new Array<Array<T>>();
    
    for (let key in dictionary)
    {
        if (!dictionary.hasOwnProperty(key))
            continue;
        
        resultArray.push(dictionary[key]);
    }
    
    return resultArray;
}

export function distinct<T, U extends string>(array: Array<T>, groupKeyMap: (item: T) => U): Array<T>
{
    return groupBy(array, groupKeyMap)
        .flatMap(group => group[0]);
}