import {Outlet} from "../types";

import bandcampIcon from "./bandcamp.svg";
import gummIcon from "./gumm.svg";
import twitterIcon from "./twitter.svg";
import youtubeIcon from "./youtube.svg";

const bandcamp: Outlet = 
{
    description: "Music",
    icon: { url: bandcampIcon },
    url: "https://www.hubol.bandcamp.com"
};

const twitter: Outlet =
{
    icon: { url: twitterIcon },
    url: "https://www.twitter.com/hubol",
    description: "Spam"
};

const gumm: Outlet =
{
    description: "Performance art",
    icon: { url: gummIcon},
    url: "http://www.gumm.band"
};

const youtube: Outlet =
{
    description: "Video",
    icon: { url: youtubeIcon },
    url: "https://www.youtube.com/channel/UCWs2A5qAG9ylDMPDgFITgfg"
};

export const outlets: Array<Outlet> = [ bandcamp, twitter, gumm, youtube ]