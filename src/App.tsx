import React from 'react';
// import logo from './logo.svg';
import './App.css';
import {ProjectExpandedView} from "./Views/ProjectExpandedView";
import {ConnectedProjectsListView} from "./Views/ProjectsListView";
import {PortfolioState} from "./Store/store";
import {Project} from "./types";
import {connect} from "react-redux";
import {getExpandedProject} from "./Store/projects";
import {OutletsListView} from "./Views/OutletsListView";
import {outlets} from "./Outlets/outlets";

interface Props
{
  expandedProject: Project | null;
}

export const App: React.FC<Props> = (props) => {
  return <>
    <OutletsListView outlets={outlets}/>
    <ConnectedProjectsListView />
    { props.expandedProject && <ProjectExpandedView project={props.expandedProject} />}
  </>
};

const mapPortfolioState = (portfolioState: PortfolioState) => ({
  expandedProject: getExpandedProject(portfolioState.router)
});

export const ConnectedApp =
    connect( mapPortfolioState )(App);
